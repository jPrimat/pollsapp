package fr.exercice.pollsapp.configs;


import fr.exercice.pollsapp.models.User;
import fr.exercice.pollsapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Security extends WebSecurityConfigurerAdapter {

    @Autowired
    UserService us;

    private final RequestMatcher ADMIN = new OrRequestMatcher(
            new AntPathRequestMatcher("/users/**"),
            new AntPathRequestMatcher("/users/**", "PUT"),
            new AntPathRequestMatcher("/users/**", "DELETE"),
            new AntPathRequestMatcher("/polls/**", "PUT"),
            new AntPathRequestMatcher("/polls/**", "DELETE")
    );

    private final RequestMatcher ALLOWED_URL = new OrRequestMatcher(
            new AntPathRequestMatcher("/users", "POST"),
            new AntPathRequestMatcher("/polls", "GET"),
            new AntPathRequestMatcher("/polls/**", "GET"),
            new AntPathRequestMatcher("/polls/*/up", "POST"),
            new AntPathRequestMatcher("/polls/*/down", "POST")
    );

    private final RequestMatcher AUTHENTICATED_URL = new OrRequestMatcher(
            new AntPathRequestMatcher("/polls", "POST")
    );

    @Bean
    public UserDetailsService userDetailsService(){
        return (email) -> {
            System.out.println("test call function");
            User u = us.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("There is no email corresponding with " + email));
            System.out.println("user" + u);
            return new org.springframework.security.core.userdetails.User(u.getEmail(), u.getPassword(), AuthorityUtils.createAuthorityList(u.getRole()));
        };
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("GET","POST","PUT","UPDATE","DELETE","OPTIONS")
                        .allowedOrigins("http://localhost:4200");
            }
        };
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //This will configure spring security to never create a cookie, every request must reauthenticate.
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().formLogin().disable().logout().disable().csrf().disable().authorizeRequests()
                .requestMatchers(ADMIN).access("hasRole('ADMIN')")
                .requestMatchers(ALLOWED_URL).permitAll()
                .requestMatchers(AUTHENTICATED_URL).authenticated()
                .antMatchers("/**").denyAll().and().cors().and().httpBasic();
    }
}
