package fr.exercice.pollsapp.controllers;

import fr.exercice.pollsapp.models.User;
import fr.exercice.pollsapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;



@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService us;

    @GetMapping("")
    private Collection<User> findAll(){
        return us.findAll();
    }

    @GetMapping("{id}")
    public User findById(@PathVariable int id){
        return us.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no user corresponding with this " + id));
    }

    @PostMapping("")
    public void save(@Valid @RequestBody User u) {
        us.save(u);
    }

    @PutMapping("{id}")
    public void update(@PathVariable int id, @Valid @RequestBody User u) {
        if(u.getId() != id) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The ids don\'t match");
        us.update(u);
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable int id){
        us.deleteById(id);
    }
}
