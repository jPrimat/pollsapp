package fr.exercice.pollsapp.controllers;


import fr.exercice.pollsapp.models.Poll;
import fr.exercice.pollsapp.services.PollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;

@RestController
@RequestMapping("/polls")
public class PollController {

    @Autowired
    private PollService ps;

    @GetMapping("")
    private Collection<Poll> findAll(){
        return ps.findAll();
    }

    @GetMapping("{id}")
    public Poll findById(@PathVariable int id){
        return ps.findById(id).get();
    }

    @PostMapping("")
    public void save(@Valid @RequestBody Poll p) throws IOException {
        ps.save(p);
    }

    @PutMapping("{id}")
    public void update(@PathVariable int id, @Valid @RequestBody Poll p){
        if(p.getId() != id) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The ids don't match");
        ps.update(p);
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable int id){
        ps.deleteById(id);
    }

    @PostMapping("{id}/up")
    public void upVote(@PathVariable int id){
        ps.upVote(id);
    }

    @PostMapping("{id}/down")
    public void downVote(@PathVariable int id){
        ps.downVote(id);
    }

}
