package fr.exercice.pollsapp.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Poll {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private int id;

    @NotBlank
    private String question;

    @Min(value = 0)
    private int positiveVotes;

    @Min(value = 0)
    private int negativeVotes;

    @ManyToOne
    private User author;

}
