package fr.exercice.pollsapp.services;

import fr.exercice.pollsapp.models.User;
import fr.exercice.pollsapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository ur;

    public Optional<User> findByEmail(String email){
        return ur.findByEmail(email);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public Optional<User> findById(int id){
        return ur.findById(id);
    }

    public Collection<User> findAll(){
        return ur.findAll();
    }

    @Transactional
    public void save(User u)  {
        ur.save(u);
    }

    @Transactional
    public void update(User u)  {
        ur.save(u);
    }

    @Transactional
    public void deleteById(int id)  {
        delete(ur.findById(id).get());
    }

    @Transactional
    public void delete(User u)  {
        ur.delete(u);
    }
}
