package fr.exercice.pollsapp.services;

import fr.exercice.pollsapp.models.Poll;
import fr.exercice.pollsapp.repositories.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

@Service
public class PollService {

    @Autowired
    private PollRepository pr;

    public Optional<Poll> findById(int id){
        return pr.findById(id);
    }

    public Collection<Poll> findAll(){
        return pr.findAll();
    }

    @Transactional
    public void save(Poll p) throws IOException {
        pr.save(p);
    }

    @Transactional
    public void update(Poll p){
        pr.save(p);
    }

    @Transactional
    public void deleteById(int id) {
        delete(pr.findById(id).get());
        throw new NullPointerException("Id Not Found");
    }

    @Transactional
    public void delete(Poll p) {
        pr.delete(p);
    }

    @Transactional
    public void upVote(int id){
      Poll p = pr.findById(id).get();
      p.setPositiveVotes(p.getPositiveVotes() + 1);
      pr.save(p);
    }

    @Transactional
    public void downVote(int id){
        Poll p = pr.findById(id).get();
        p.setNegativeVotes(p.getNegativeVotes() - 1);
        pr.save(p);
    }
}
