package fr.exercice.pollsapp.aspects;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggerAspect {


    @AfterThrowing(pointcut = "execution(* fr.exercice.pollsapp.services.*.*(..))", throwing = "e")
    public void exceptionLog(Exception e) {
        System.err.println("ERROR " + e.getClass().getSimpleName() + " : " + e.getMessage());
    }

}
